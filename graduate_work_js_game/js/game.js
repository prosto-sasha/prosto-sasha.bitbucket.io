'use strict';

// --------------------------------------------------------------------- [Class Vector] ---------------------------------------------------

class Vector {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  plus(vector) {
    if (!(vector instanceof Vector)) {
      throw new Error('Можно прибавлять к вектору только вектор типа Vector');
    }

    return new Vector(this.x + vector.x, this.y + vector.y);
  }

  times(multiple) {
    const timesX = this.x * multiple;
    const timesY = this.y * multiple;
    return new Vector(timesX, timesY);
  }
}

// --------------------------------------------------------------------- [Class Actor] ---------------------------------------------------

class Actor {
  constructor(pos = new Vector(), size = new Vector(1,1), speed = new Vector()) {
    if (!(pos instanceof Vector) || !(size instanceof Vector) || !(speed instanceof Vector)) {
      throw new Error('В параметры можно передать только объект типа Vector');
    }

    this.pos = pos;
    this.size = size;
    this.speed = speed;
  }

  act () {}

  get left() {
    return this.pos.x;
  }

  get top() {
    return this.pos.y;
  }

  get right() {
    return this.left + this.size.x;
  }

  get bottom() {
    return this.top + this.size.y;
  }

  get type() {
    return 'actor';
  }

  isIntersect(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('Нужно добавить параметр или в параметре объект не типа Vector');
    }
    
    if (this !== actor) {
      if (this.right > actor.left && this.left < actor.right && this.bottom > actor.top && this.top < actor.bottom) {
        return true;
      }
    }

    return false;
  }
}

// --------------------------------------------------------------------- [Class Level] ---------------------------------------------------

class Level {
  constructor(grid = [], actors = []) {
    this.grid = grid;
    this.actors = actors;
    this.player = this.actors.find((actor) => actor.type === 'player');
    this.height = grid.length;
    this.status = null;
    this.finishDelay = 1;
  }

  get width() {
    return this.height > 0 ? Math.max(...([...this.grid].map((item) => item.length))) : 0;
  }

  isFinished() {
    return this.status !== null && this.finishDelay < 0;
  }

  actorAt(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('Нужно добавить параметр или в параметре объект не типа Vector');
    }

    return this.actors.find((el) => el.isIntersect(actor));
  }

  // ??????????
  obstacleAt(pos, size) {
    if (!(pos instanceof Vector) || !(size instanceof Vector)) {
      throw new Error('В параметры передан объект не типа Vector');
    }

    const currentActor = new Actor(pos, size);

    let typeElementGrid = '';

    if (currentActor.left < 0 || currentActor.right > this.width || currentActor.top < 0) {
      typeElementGrid = 'wall';
    } else if (currentActor.bottom > this.height) {
      typeElementGrid = 'lava';
    } else {
      typeElementGrid = undefined;
    }

    if (this.grid[pos.y] !== undefined) {
      if (this.grid[pos.y][pos.x] !== undefined) {
        typeElementGrid = this.grid[pos.y][pos.x] === 'wall' ? 'wall' : 'lava';
      }
    }

    return typeElementGrid;
  }

  removeActor(actor) {
    if (actor !== undefined) {
      this.actors.splice(this.actors.indexOf(actor), 1);
    }
  }

  noMoreActors(typeActor = '') {
    if (typeActor === '') {
      return this.actors.length === 0;
    } else {
      return this.actors.filter((el) => el.type === typeActor).length === 0;
    }
  }

  playerTouched(type, coin) {
    switch(type) {
      case 'lava':
      case 'fireball': 
        this.status = 'lost';
        break;
      case 'coin':
        if (coin !== undefined) {
          this.removeActor(coin);
        }

        const result = this.actors.find((el) => el.type === 'coin');

        if (result === undefined) {
          this.status = 'won';
        }
        break;
      default:
        return;
    }
  }
}

// --------------------------------------------------------------------- [Class LevelParse] ---------------------------------------------------

class LevelParser {
  constructor(set) {
    this.set = set;
  }

  actorFromSymbol(symbol) {
    if (symbol && this.set !== undefined) {
      return this.set[symbol];
    }
  }

  obstacleFromSymbol(symbol) {
    if (symbol) {
      switch (symbol) {
        case 'x': 
          return 'wall';
        case '!':
          return 'lava';
        default:
          return;
      }
    }
  }

  createGrid(list) {
    return list.map((str) => [...str]).map((item) => {
      return item.map((el) => this.obstacleFromSymbol(el));
    });
  }

  createActors(strList) {
    const actionObjList = [];

    strList.forEach((itemX, i) => {
      [...itemX].forEach((itemY, j) => {
        if (this.actorFromSymbol(itemY) !== undefined) {
          actionObjList.push(new (this.actorFromSymbol(itemY))(new Vector(j, i)));
        }
      });
    });

    return actionObjList;
  }

  parse(plan) {
    return new Level(this.createGrid(plan), this.createActors(plan));
  }
}

// --------------------------------------------------------------------- [Class Fireball] ---------------------------------------------------

class Fireball extends Actor {
  constructor(pos = new Vector(0, 0), speed = new Vector(0, 0), size = new Vector(1, 1)) {
    super();
    this.pos = pos;
    this.speed = speed;
    this.size = size;
  }

  get type() {
    return 'fireball';
  }

  getNextPosition(time = 1) {
    return new Vector(this.pos.x + (this.speed.x * time), this.pos.y + (this.speed.y * time));
  }

  handleObstacle() {
    const MULTIPLE = -1;
    const thisSpeedX = this.speed.x * MULTIPLE;
    const thisSpeedY = this.speed.y * MULTIPLE;
    this.speed = new Vector(thisSpeedX, thisSpeedY);
  }

  act(time, level) {
    const nextPosition = this.getNextPosition(time);

    if (level.obstacleAt(this.pos, this.size)) {
      this.handleObstacle();
    } else {
      this.pos = nextPosition;
    }
  }
}

// --------------------------------------------------------------------- [Class HorizontalFireball] ---------------------------------------------------

class HorizontalFireball extends Fireball {
  constructor(pos, speed = new Vector(2, 0), size) {
    super(pos, speed, size);
  }
}

// --------------------------------------------------------------------- [Class VerticalFireball] ---------------------------------------------------

class VerticalFireball extends Fireball {
  constructor(pos, speed = new Vector(0, 2), size) {
    super(pos, speed, size);
  }
}

// --------------------------------------------------------------------- [Class FireRain] ---------------------------------------------------

class FireRain extends Fireball {
  constructor(pos, speed = new Vector(0, 3), size) {
    super(pos, speed, size);
    this.currentPos = pos;
  }

  handleObstacle() {
    this.speed = new Vector(this.speed.x, this.speed.y);
    this.pos = new Vector(this.currentPos.x, this.currentPos.y);
  }
}

// --------------------------------------------------------------------- [Class Coin] ---------------------------------------------------

class Coin extends Actor {
  constructor(pos = new Vector(), size = new Vector(0.6, 0.6), speed) {
    super(pos, size, speed);
    this.pos = pos.plus(new Vector(0.2, 0.1));
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.spring = Math.floor(Math.random() * (2 * Math.PI));
  }

  get type() {
    return 'coin';
  }

  updateSpring(time = 1) {
    this.spring += this.springSpeed * time;
  }

  getSpringVector() {
    const newY = Math.sin(this.spring) * this.springDist;
    return new Vector(0, newY);
  }

  getNextPosition(time = 1) {
    this.updateSpring(time);
    return this.pos.plus(this.getSpringVector());
  }
}

// --------------------------------------------------------------------- [Class Player] ---------------------------------------------------

class Player extends Actor {
  constructor(pos = new Vector(0, -0.5), size = new Vector(1.2, 1.2), speed) {
    super(pos, size, speed);
  }

  get type() {
    return 'player';
  }
}

// --------------------------------------------------------------------- Start game ---------------------------------------------------

const actorDict = {
  'x': Actor,
  '!': Actor,
  '@': Player,
  'o': Coin,
  '=': HorizontalFireball,
  '|': VerticalFireball,
  'v': FireRain
};

const parser = new LevelParser(actorDict);

loadLevels().then(schemas => runGame(JSON.parse(schemas), parser, DOMDisplay).then(() => alert('Поздравляю! Вы прошли игру!')));